using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
//using UnityEditor.PackageManager.UI;

//#if UNITY_WSA && ENABLE_WINMD_SUPPORT
#if WINDOWS_UWP
using Windows.Storage;
#endif

public class SaveScript : MonoBehaviour
{

    private static string Save_Directory_Path = "U:\\Users\\VRLAB\\Documents\\";
    //private static string Save_Directory_Path = "ms-appdata:///local/";

    public static void Save_Test_Data(string testId, float[,] data)
    {
        //Target size 0, Position im Raum 1-3, Headposition im Raum bei Spawn 4-6, Zeitstempel vom Ziel spawn 7, Zeitstempel vom ausw�hlen 8, Blickrichtung beim spawn 9-12, Vektor zum Ziel 13-15
        string fileDescription = "targetSize; targetPosX; targetPosY; targetPosZ; HeadPosX; HeadPosY; HeadPosZ; tsSpawn; tsHit; gazeX; gazeY; gazeZ; targetDirX; targetDirY; targetDirZ";
        //float.ToString("0.00")
        var date = DateTime.Now;
        //date.Hour;        date.Minute;
        string[] lines = new string[data.GetLength(0) + 1];
        lines[0] = fileDescription;
        for (int i = 0; i < data.GetLength(0); i++)
        {
            string line = "";
            for (int j = 0; j < data.GetLength(1); j++)
            {
                float dataPoint = data[i, j];
                if (line.Length == 0)
                {
                    line += dataPoint.ToString("0.0000");
                }
                else
                {
                    line += ";" + dataPoint.ToString("0.0000");
                }
            }
            lines[i + 1] = line;
        }
        string fileName = testId + "_" + date.Hour.ToString() + "_" + date.Minute.ToString() + "_" + date.Second.ToString() + ".csv";
        writeLinesToFile(lines, fileName);
    }
#if !WINDOWS_UWP
    private static async void writeLinesToFile(string[] lines, string fileName)
    {
        //save file if on dev pc
        if (Directory.Exists("E:\\Huc4\\Wissenschaftliche_Vertiefung"))
        {
            Debug.Log("Creating folder on PC...");
            string directoryPath = "E:\\Huc4\\Wissenschaftliche_Vertiefung\\FittData";
            Directory.CreateDirectory(directoryPath);
            using StreamWriter sw_file = new StreamWriter(Path.Combine(directoryPath, fileName));

            foreach (string line in lines)
            {
                await sw_file.WriteLineAsync(line);
            }
        }
        else
        {
            Debug.Log("Saving file on hololens");
            string directoryPath = "U:\\Users\\VRLAB\\Documents\\FittData";
            Directory.CreateDirectory(directoryPath);
            using StreamWriter hl_file = new StreamWriter(Path.Combine(directoryPath, fileName));

            foreach (string line in lines)
            {
                await hl_file.WriteLineAsync(line);
            }
        }
    }
#endif
    //#if UNITY_WSA && ENABLE_WINMD_SUPPORT
#if WINDOWS_UWP
    private static async void writeLinesToFile(string[] lines, string fileName)
    {
        Debug.Log("Conditional code exists");
        //Known Folder is part of Namespace Windows.Storage
        StorageFolder storageFolder = KnownFolders.PicturesLibrary;
        StorageFile file = await storageFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
        // Append a list of strings, one per line, to the file
        var listOfStrings = new List<string>(lines);
        await Windows.Storage.FileIO.AppendLinesAsync(file, listOfStrings);
    }
#endif
}
