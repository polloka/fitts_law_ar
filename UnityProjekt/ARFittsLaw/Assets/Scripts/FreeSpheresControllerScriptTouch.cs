using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit;

public class FreeSpheresControllerScriptTouch : MonoBehaviour
{

    public GameObject targetPrefab;

    public GameObject testDonePrefab;

    public GameObject startButton;

    public GameObject testArea;

    public int numberOfTests = 25;

    private GameObject currentTarget;

    //Target size 0, Position im Raum 1-3, Handposition im Raum bei Spawn 4-6, Zeitstempel vom Ziel spawn 7, Zeitstempel vom ausw�hlen 8, Blickrichtung beim spawn 9-12, Vektor zum Ziel 13-15
    private float[,] testDataArray = new float[25, 15];

    private int testCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        Button button = startButton.GetComponent(typeof(Button)) as Button;
        
        if (button != null)
            //Debug.Log("button component found");
            button.onClick.AddListener(StartTest);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartTest()
    {
        testDataArray = new float[25, 15];
        startButton.SetActive(false);
        testCounter = 0;
        SpawnTarget();
    }

    void SpawnTarget()
    {
        if(testCounter < numberOfTests)
        {
            Vector3 spawnPosition = testArea.transform.position;
            spawnPosition.x = spawnPosition.x + Random.Range(-0.5f, 0.5f);
            spawnPosition.z = spawnPosition.z + Random.Range(-0.75f, 0.75f);
            spawnPosition.y = spawnPosition.y + Random.Range(0.75f, 1.75f);
            currentTarget = Instantiate(targetPrefab, spawnPosition, Quaternion.identity);
            float scale = Random.Range(0.05f, 0.25f);
            currentTarget.transform.localScale = new Vector3(scale, scale, scale);
            // Add and configure the touchable
                      
            // Call Target Clicked on pointer down
            //var pointerHandler = currentTarget.AddComponent<PointerHandler>();
            //pointerHandler.OnPointerDown.AddListener((e) => TargetClicked());

            var ph = currentTarget.AddComponent<PointerHandler>();
            ph.OnPointerDown.AddListener(OnPointerDown);
            //pointerHandler.OnPointerUp.AddListener((e) => material.color = Color.magenta);
            //var touchButton = currentTarget.AddComponent<Button>();
            //touchButton.onClick.AddListener(TargetClicked);

            //save Data at instantiation: Target size 0, Target-Position 1-3, Handposition at spawn 4-6, Timestamp spawn, Gaze_Direction at spawn 9-11, Vektor to target Ziel 12-14
            Vector3 gazeDirection = Camera.main.transform.forward;
            Vector3 vectorToTarget = spawnPosition - Camera.main.transform.position;
            testDataArray[testCounter, 0] = scale;
            testDataArray[testCounter, 1] = spawnPosition.x - testArea.transform.position.x;
            testDataArray[testCounter, 2] = spawnPosition.y - testArea.transform.position.y;
            testDataArray[testCounter, 3] = spawnPosition.z - testArea.transform.position.z;
            testDataArray[testCounter, 4] = Camera.main.transform.position.x - testArea.transform.position.x;
            testDataArray[testCounter, 5] = Camera.main.transform.position.y - testArea.transform.position.y;
            testDataArray[testCounter, 6] = Camera.main.transform.position.z - testArea.transform.position.z;
            testDataArray[testCounter, 7] = Time.time;
            //
            testDataArray[testCounter, 9] = gazeDirection.x;
            testDataArray[testCounter, 10] = gazeDirection.y;
            testDataArray[testCounter, 11] = gazeDirection.z;
            testDataArray[testCounter, 12] = vectorToTarget.x;
            testDataArray[testCounter, 13] = vectorToTarget.y;
            testDataArray[testCounter, 14] = vectorToTarget.z;

            //IndexTip	11
            testCounter++;
        }
        else
        {
            SaveTestData();
        }
    }

    void TargetClicked()
    {
        //save data at click: timestamp of click 8
        testDataArray[testCounter - 1, 8] = Time.time;

        //spawn next Target
        Destroy(currentTarget);
        Invoke("SpawnTarget", 2);
    }

    void SaveTestData()
    {
        //Write Data to file and reset test
        startButton.SetActive(true);
        testCounter = 0;

        //Spawn test done sign and destroy it
        var destroyTime = 5;
        Vector3 spawnPosition = testArea.transform.position;
        spawnPosition.y = spawnPosition.y + 1.5f;
        GameObject testDoneText = Instantiate(testDonePrefab, spawnPosition, Quaternion.identity);
        Destroy(testDoneText, destroyTime);

        SaveScript.Save_Test_Data("FT", testDataArray);
    }

    private bool IsDirectGrab(IMixedRealityPointer p)
    {
        return p is SpherePointer;
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        if (IsDirectGrab(eventData.Pointer))
        {
            if (eventData.Pointer is MonoBehaviour monobehaviorPointer)
            {
                Debug.Log("Object was grabed");
                TargetClicked();
            }
            //else not our desired interaction type
        }
    }
}
