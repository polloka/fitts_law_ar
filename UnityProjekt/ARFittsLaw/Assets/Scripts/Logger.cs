using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
//Shoutout @MichaelPietschmann
public class Logger : MonoBehaviour
{
    public TMP_Text ouputTM;
    
    // Start is called before the first frame update
    void Start()
    {
        this.Print(this, "Logger registriert!");
    }

    public void OnEnable()
    {
        Application.logMessageReceived += LogMessage;
    }

    public void OnDisable()
    {
        Application.logMessageReceived -= LogMessage;
    }

    public void Print(Object source, string content)
    {
        ouputTM.text += "\n";
        if(source != null)
        {
            ouputTM.text += source.GetType() + ": " + content;
        }
        else
        {
            ouputTM.text += "SomeClass" + ": " + content;
        }
        
    }

    private void LogMessage(string message, string stackTrace, LogType type)
    {
        if(stackTrace != null && type == LogType.Exception)
        {
            Print(this, type.ToString() + ": " + message + "\nStackTrace:" + stackTrace.Substring(0, 300) + "[...]");
        }
        else
        {
            Print(this, type.ToString() + ": " + message);
        }
        
    }
}
