var current_circle;
var data = [];
var numberTargetsHit = 0;
var numberOfTotalTargets = 0;

var canvasWidth = 0;
var canvasHeight = 0;
var canvas;
var context;

var mouseXAtSpawn;
var mouseYAtSpawn;
var mouseX;
var mouseY;
var timeStampSpawn;

//const fs = require('fs')
//executes myFunction after milliseconds with the args in s
//setTimeout(myFunction, milliseconds, arg(s))

function startTest() {
    //prep variables
    canvas = document.getElementById('canvas');
    context = canvas.getContext('2d');
    console.log(context);

    numberOfTotalTargets = document.getElementById("targetNumber").value;
    numberTargetsHit = 0;
    data = [];
    setTimeout(spawnCircle, 1000);
}

function spawnCircle() {
    
    var circle_x = randomIntFromInterval(0, canvas.width);
    var circle_y = randomIntFromInterval(0, canvas.height);
    var circle_radius = randomIntFromInterval(10, 50);
    timeStampSpawn = Date.now();
    mouseXAtSpawn = mouseX;
    mouseYAtSpawn = mouseY;
    console.log("Making circle at position " + circle_x + "/" + circle_y + " and radius " + circle_radius);    
    current_circle = Circle({ x: circle_x, y: circle_y, radius: circle_radius });

    current_circle.draw();
}

function saveData() {
    console.log(data);
    
    let platform = document.getElementById("platformSelector").value;
    let testId = document.getElementById("testID").value;
    let date = new Date();
    let fileName = testId + "_" + platform + "_" + date.getHours() + "_" + date.getMinutes() + "_" + date.getSeconds() + ".csv";
    let file_content = "targetNumber, xPosTarget, yPosTarget, radiusTarget, mouseXAtSpawn, mouseYAtSpawn, timeStampSpawn, timeStampClick\n";

    data.forEach(element => {
        element.forEach(data_point => {
            file_content = file_content + data_point + ",";
        });
        if(file_content.charAt( file_content.length-1 ) == ",") {
            file_content = file_content.slice(0, -1);
        }
        file_content = file_content + "\n";
    });
    console.log('Saving file...');
    var myFile = new File([file_content],fileName, {type: "text/plain;charset=utf8"});
    saveAs(myFile);
    console.log('File saved.');

    /*fs.writeFile(fileName, file_content, (err) => {
        // In case of a error throw err.
        if (err) throw err;
    })*/
}

function hideTestDoneMsg(){
    document.getElementById("testdonemsg").style.display = "none";
}

function abortTest() {
    //console.log(data)

}

function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
}


//circle creation and click detection
//circles[circles.length] = Circle({ x: 231, y: 278, label: "1" });

function Circle(I) {
    I.draw = function () {
        context.beginPath();
        context.arc(I.x, I.y, I.radius, 0, 2 * Math.PI, false);
        context.fillStyle = 'red';
        context.fill();
        context.lineWidth = 2;
        context.strokeStyle = '#003300';
        context.stroke();
        context.fillStyle = 'black';
    };
    return I;
}
// ----------

function updateMousePosition(evt) { 
    mouseX = evt.offsetX || evt.pageX; 
    mouseY = evt.offsetY || evt.pageY;
    //console.log(mouseX + ":" + mouseY);
}

function clickIt(evt) {
    var i, xPos, yPos, xDiff, yDiff, dist, cX, cY;
    evt = evt || event;
    xPos = evt.offsetX || evt.pageX;
    yPos = evt.offsetY || evt.pageY;
    // check posn against centres       
    cX = current_circle.x;
    cY = current_circle.y;
    xDiff = Math.abs(cX - xPos);
    yDiff = Math.abs(cY - yPos);
    dist = Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
    if (dist <= current_circle.radius) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        timeStampClick = Date.now();
        //adding to data: targetNumber, xPosTarget, yPosTarget, radiusTarget, mouseXAtSpawn, mouseYAtSpawn, timeStampSpawn, timeStampClick
        data.push([numberTargetsHit, current_circle.x, current_circle.y, current_circle.radius, mouseXAtSpawn, mouseYAtSpawn, timeStampSpawn, timeStampClick]);
        numberTargetsHit = numberTargetsHit + 1;
        console.log("Target hit");
        if(numberTargetsHit >= numberOfTotalTargets){
            document.getElementById("testdonemsg").style.display = "block";
            saveData();
            setTimeout(hideTestDoneMsg,2000);
        }else{
            setTimeout(spawnCircle, 1000);
        }
    }
}
